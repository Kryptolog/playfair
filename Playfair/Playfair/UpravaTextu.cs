﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Playfair
{
    static class UpravaTextu
    {
        //vytvori z klice klicovou abecedu
        public static string KlicAbc(string key)
        {
            string[] sourceArray = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "Z" };
            string klicovaAbeceda="";

            key=NachystejStringEnc(key);// upravim na velka pismena atd.


            for (int i = 0; i < key.Length; i++)//pokud jsou v klici duplicitni znaky tak je odstrani
            {
                for (int j = 0; j < key.Length; j++)
                {
                    if (i != j)
                    {
                        if (key[i] == key[j])
                        {
                            key = key.Remove(j,1);
                        }
                    }
                }
            }

            klicovaAbeceda = key; //momentalne je klicova abeceda pouze klic

            for (int l = 0; l < sourceArray.Length; l++)//k abecede pridáme celou abecedu
            {
                klicovaAbeceda = klicovaAbeceda.Insert(klicovaAbeceda.Length, sourceArray[l]);
            }

            string konecny = String.Join("",klicovaAbeceda.Distinct());// odebereme duplicitni znaky (jina metoda odebrani nez predtim, je kradsi)

            return (konecny);
        }
        
        //metoda pro upravu textu do petic
        public static string Petice(string text)
        {
            for (int i = 5; i <= text.Length; i += 5)//incrementujeme po 5
            {
                text = text.Insert(i, " ");
                i++;//nutné protoze pokazde pridame - takze text je 1 delsi
            }
            char last = text[text.Length - 1];//posledni
            if (last == ' ')//kontroluje zda je posledni zank ' '
            {
                text = text.Substring(0, text.Length - 1);//odstrani pomlcku
            }
            return (text);
        }

        //metoda pro upravu textu z petic na normalni text
        public static string ZrusPetice(string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == ' ')
                {
                    text = text.Remove(i, 1);//odebere mezeru
                }
            }
            return (text);
        }


        //Chysta string pro sifrovani
        public static string NachystejStringEnc(string clearText)
        {
            clearText = clearText.ToUpper();
            clearText = clearText.Replace('W', 'V');
            clearText = clearText.Replace(" ", "QMEZERAS");
            clearText = OdstranSpecialky(clearText);
            clearText = RemoveDiacritics(clearText);
            clearText = PrevodCiselEncrypt(clearText);

            return clearText;

        }

        //metoda na odstranění diakritiky - našli jsme
        public static string RemoveDiacritics(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.Normalize(NormalizationForm.FormD);
            var chars = text.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark).ToArray();
            return new string(chars).Normalize(NormalizationForm.FormC);
        }

        public static string NachystejStringDec(string decrypted)
        {
            decrypted = decrypted.Replace("QMEZERAS", " ");
            decrypted = PrevodCiselDecrypt(decrypted);

            return decrypted;
        }

        //Prevadi cisla na pismena s vyuzitim indexu pole
        public static string PrevodCiselEncrypt(string clearText)
        {
            string[] cisla = new string[]
            {
                "XNULAQ","XJEDNAQ", "XDVAQ", "XTRIQ", "XCTYRIQ", "XPETQ", "XSESTQ", "XSEDMQ", "XOSMQ", "XDEVETQ"
            };
            //vymeni ve stringu cisla za slova z pole cisla
            for (int i = 0; i < cisla.Length; i++)
            {
                clearText = clearText.Replace($"{i}", $"{cisla[i]}");
            }

            return clearText;
        }

        public static string PrevodCiselDecrypt(string decrypted)
        {
            string[] cisla = new string[]
            {
                "XNULAQ","XJEDNAQ", "XDVAQ", "XTRIQ", "XCTYRIQ", "XPETQ", "XSESTQ", "XSEDMQ", "XOSMQ", "XDEVETQ"
            };
            for (int i = 0; i < cisla.Length; i++)
            {
                decrypted = decrypted.Replace($"{cisla[i]}", $"{i}");
            }

            return decrypted;
        }

        //Nebyl cas hledat funkce, takze zbaveni se "balastu" je nejspis dost znasilnene
        public static string OdstranSpecialky(string clearText)
        {
            clearText = clearText.Replace("ů", "");
            clearText = clearText.Replace("!", "");
            clearText = clearText.Replace("?", "");
            clearText = clearText.Replace(",", "");
            clearText = clearText.Replace(".", "");
            clearText = clearText.Replace("/", "");
            clearText = clearText.Replace("\\", "");
            clearText = clearText.Replace("|", "");
            clearText = clearText.Replace(";", "");
            clearText = clearText.Replace("'", "");
            clearText = clearText.Replace("[", "");
            clearText = clearText.Replace("]", "");
            clearText = clearText.Replace("=", "");
            clearText = clearText.Replace("-", "");
            clearText = clearText.Replace("_", "");
            clearText = clearText.Replace("0", "");
            clearText = clearText.Replace("(", "");
            clearText = clearText.Replace(")", "");
            clearText = clearText.Replace("*", "");
            clearText = clearText.Replace("&", "");
            clearText = clearText.Replace("^", "");
            clearText = clearText.Replace("%", "");
            clearText = clearText.Replace("$", "");
            clearText = clearText.Replace("#", "");
            clearText = clearText.Replace("{", "");
            clearText = clearText.Replace("}", "");
            clearText = clearText.Replace("`", "");
            clearText = clearText.Replace("~", "");
            clearText = clearText.Replace("ˇ", "");
            return clearText;
        }
    }
}
