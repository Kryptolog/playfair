﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Playfair
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.ForeColor = Color.Orange;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (textBox1.Text == "" || textBox1.Text == " ")
            {
                MessageBox.Show("Zadejete klíč!", "Žádný klíč", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox1.Text = "Zde zadejte klíč";
                textBox1.ForeColor = Color.Red;

            }
            else if (textBox1.Text.Length < 6)
            {
                MessageBox.Show("Zadejete klíč, který má alespoň 6 znaků!", "Krátký klíč", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                
                textBox1.ForeColor = Color.Red;

            }
            else if (textBox2.Text == "" || textBox2.Text == " ")
            {
                MessageBox.Show("Zadejete text!", "Žádný text", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox2.Text = "Zde zadejte text";
                textBox2.ForeColor = Color.Red;
            }
            else if (textBox1.Text.Contains(" "))
            {
                MessageBox.Show("Zadejete klíč bez mezery!", "Klíč bez mezery", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox1.ForeColor = Color.Red;
            }
            else
            {
                textBox1.ForeColor = Color.LimeGreen;
                textBox2.ForeColor = Color.LimeGreen;
                string encrypted = Encryption.Encrypt(textBox2.Text, textBox1.Text);
                textBox3.Text = UpravaTextu.Petice(encrypted);
                textBox4.Text = textBox3.Text;
                textBox4.ForeColor = Color.LimeGreen;
                button2.Enabled = true;

            }

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string klic = textBox1.Text;
            string klicovaAbc = UpravaTextu.KlicAbc(klic);

            char[,] arrayTable = Encryption.Tabulka(klicovaAbc);
            

            dataGridView1.ColumnCount = arrayTable.GetLength(1);
            for (int i = 0; i < arrayTable.GetLength(0); i++)
            {
                string[] oneRow = new string[arrayTable.GetLength(1)];
                for (int j = 0; j < arrayTable.GetLength(1); j++)
                {
                    oneRow[j] = Convert.ToString(arrayTable[i, j]);
                }
                dataGridView1.Rows.Add(oneRow);
            }
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.AllowUserToAddRows = false;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            textBox4.ForeColor = Color.Orange;
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            textBox5.ForeColor = Color.Orange;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox5.Text == "" || textBox5.Text == " ")
            {
                MessageBox.Show("Zadejete klíč!", "Žádný klíč", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox5.Text = "Zde zadejte klíč";
                textBox5.ForeColor = Color.Red;

            }
            else if (textBox5.Text.Length < 6)
            {
                MessageBox.Show("Zadejete klíč, který má alespoň 6 znaků!", "Krátký klíč", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               
                textBox5.ForeColor = Color.Red;

            }
            else if (textBox5.Text.Contains(" "))
            {
                MessageBox.Show("Zadejete klíč bez mezery!", "Klíč bez mezery", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox5.ForeColor = Color.Red;
            }
            else if (textBox4.Text == "" || textBox4.Text == " " || textBox4.Text.Length == 1)
            {
                MessageBox.Show("Zadejete text!", "Žádný text", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox4.Text = "Zde zadejte zašifrovaný text";
                textBox4.ForeColor = Color.Red;
            }
            else
            {
                textBox4.ForeColor = Color.LimeGreen;
                textBox5.ForeColor = Color.LimeGreen;
                string toDecrypt = UpravaTextu.ZrusPetice(textBox4.Text);
                string decrypted = Decryption.Decrypt(toDecrypt, textBox5.Text);
                textBox6.Text = decrypted;
                button2.Enabled = true;
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.ForeColor = Color.Orange;
        }
    }
}
