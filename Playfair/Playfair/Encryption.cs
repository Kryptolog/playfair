﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Playfair
{
    class Encryption
    {
        string[] sourceArray = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "Z" };
        //vytvoreni tabulky
        public static char[,] Tabulka(string klicovaAbeceda)
        {
            char[,] arrayTable = new char[5, 5];
            for (int i = 0; i < arrayTable.GetLength(0); i++)
            {
                for (int j = 0; j < arrayTable.GetLength(1); j++)
                {
                    arrayTable[i, j] = Convert.ToChar(klicovaAbeceda.Substring(arrayTable.GetLength(0) * i + j, 1));
                }
            }
            return (arrayTable);
        }

        public static string Encrypt(string original, string key)
        {
            string klicovaAbc = UpravaTextu.KlicAbc(key);
            char[,] tabulka = Tabulka(klicovaAbc);
            original = UpravaTextu.NachystejStringEnc(original);
            
            //pokud jsou duplicitni dva po sobe jdoucci znaky rozdeli je X
            for (int i = 0; i < original.Length-1; i++)
            {
                if (original[i] == 'X' && original[i + 1] == 'X')
                {
                    original = original.Insert(i + 1, "Q");
                }

                if (original[i] == original[i + 1])
                {
                    original = original.Insert(i + 1, "X");
                }

            }

            //pokud lichy pocet prida X (pokud konci X tak Q)
            if (original.Length % 2 != 0)
            {
                if (original[original.Length-1]=='X')
                {
                    original = original.Insert(original.Length, "Q");
                }
                else original = original.Insert(original.Length, "X");
            }

            //sifrovani
            char[] encrypZnaky = new char[original.Length];
            for (int i = 0; i < original.Length; i+=2)
            {
                char pismeno1 = original[i];
                char pismeno2 = original[i + 1];

                int sx1=0;
                int sy1=1;
                int sx2=1;
                int sy2=0;

                for (int x = 0; x < 5; x++)
                {
                    for (int y = 0; y < 5; y++)
                    {
                        if (tabulka[x, y] == pismeno1)//najde a porovna pismeno z tabulky s pismenem z originalniho textu
                        {
                            sx1 = x;
                            sy1 = y;
                        }
                        if (tabulka[x, y] == pismeno2)//najde a porovna pismeno z tabulky s pismenem z originalniho textu
                        {
                            sx2 = x;
                            sy2 = y;
                        }
                    }
                }
                //posun pokud jsou na stejnem radku
                if (sx1 == sx2)
                {
                    if (sy1 >= 4)
                    {
                        sy1 = 0;
                        sy2++;
                    }
                    else if (sy2 >= 4)
                    {
                        sy2 = 0;
                        sy1++;
                    }
                    else
                    {
                        sy1++;
                        sy2++;
                    }
                }
                //posun pokud jsou ve stejnem sloupci
                else if (sy1 == sy2)
                {
                    if (sx1 >= 4)
                    {
                        sx1 = 0;
                        sx2++;
                    }
                    else if (sx2 >= 4)
                    {
                        sx2 = 0;
                        sx1++;
                    }
                    else
                    {
                        sx1++;
                        sx2++;
                    }
                }
                //posun poku jsou kazdy jinak
                else if (sx1 != sx2 && sy1 != sy2)
                {
                    int tmp;
                    tmp = sy1;
                    sy1 = sy2;
                    sy2 = tmp;
                }


                encrypZnaky[i] = tabulka[sx1, sy1];
                encrypZnaky[i+1] = tabulka[sx2, sy2];
            }

            string encrypted = new String(encrypZnaky);

            return (encrypted);
        }
    }
}
