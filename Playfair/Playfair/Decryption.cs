﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Playfair
{
    class Decryption
    {
        string[] sourceArray = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "Z" };
        //vytvoreni tabulky
        public static char[,] Tabulka(string klicovaAbeceda)
        {
            char[,] arrayTable = new char[5, 5];
            for (int i = 0; i < arrayTable.GetLength(0); i++)
            {
                for (int j = 0; j < arrayTable.GetLength(1); j++)
                {
                    arrayTable[i, j] = Convert.ToChar(klicovaAbeceda.Substring(arrayTable.GetLength(0) * i + j, 1));
                }
            }
            return (arrayTable);
        }

        public static string Decrypt(string original, string key)
        {
            string klicovaAbc = UpravaTextu.KlicAbc(key);
            char[,] tabulka = Tabulka(klicovaAbc);

            //desifrovani
            char[] decrypZnaky = new char[original.Length];
            for (int i = 0; i < original.Length; i += 2)
            {
                char pismeno1 = original[i];
                char pismeno2 = original[i + 1];

                int sx1 = 0;
                int sy1 = 1;
                int sx2 = 1;
                int sy2 = 0;

                for (int x = 0; x < 5; x++)
                {
                    for (int y = 0; y < 5; y++)
                    {
                        if (tabulka[x, y] == pismeno1)//najde a porovna pismeno z tabulky s pismenem z originalniho textu
                        {
                            sx1 = x;
                            sy1 = y;
                        }
                        if (tabulka[x, y] == pismeno2)//najde a porovna pismeno z tabulky s pismenem z originalniho textu
                        {
                            sx2 = x;
                            sy2 = y;
                        }
                    }
                }
                //posun pokud jsou na stejnem radku
                if (sx1 == sx2)
                {
                    if (sy1 == 0)
                    {
                        sy1 = 4;
                        sy2--;
                    }
                    else if (sy2 == 0)
                    {
                        sy2 = 4;
                        sy1--;
                    }
                    else
                    {
                        sy1--;
                        sy2--;
                    }
                }
                //posun pokud jsou ve stejnem sloupci
                else if (sy1 == sy2)
                {
                    if (sx1 == 0)
                    {
                        sx1 = 4;
                        sx2--;
                    }
                    else if (sx2 == 0)
                    {
                        sx2 = 4;
                        sx1--;
                    }
                    else
                    {
                        sx1--;
                        sx2--;
                    }
                }
                //posun poku jsou kazdy jinak
                else if (sx1 != sx2 && sy1 != sy2)
                {
                    int tmp;
                    tmp = sy1;
                    sy1 = sy2;
                    sy2 = tmp;
                }


                decrypZnaky[i] = tabulka[sx1, sy1];
                decrypZnaky[i + 1] = tabulka[sx2, sy2];
            }

            string decrypted = new String(decrypZnaky);
            decrypted = UpravaTextu.PrevodCiselDecrypt(decrypted);
            if (decrypted[decrypted.Length - 1] == 'X')
            {
                decrypted = decrypted.Remove(decrypted.Length - 1);
            }

            if (decrypted[decrypted.Length - 1] == 'Q')
            {
                decrypted = decrypted.Remove(decrypted.Length - 1);
            }

            for (int i = 0; i < decrypted.Length; i++)
            {
                if (decrypted[i] == 'X')
                {
                    if (i > 0 && i!=decrypted.Length-1)
                    {
                        if (decrypted[i - 1] == decrypted[i + 1])
                        {
                            decrypted= decrypted.Remove(i,1);
                        }
                    }
                }
                else if (decrypted[i] == 'Q')
                {
                    if (i > 0 && i != decrypted.Length - 1)
                    {
                        if (decrypted[i - 1] == decrypted[i + 1])
                        {
                            decrypted = decrypted.Remove(i, 1);
                        }
                    }
                }

            }

            decrypted = UpravaTextu.NachystejStringDec(decrypted);

            return (decrypted);
        }
    }
}
